from django.views.generic import TemplateView


class AdvertIndexView(TemplateView):
    """
        Advert page view
    """
    template_name = 'advert/index.html'

    def get(self, request, *args, **kwargs):
        return self.render_to_response({})
