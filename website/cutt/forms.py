
from django import forms
from django.views.generic import ListView
from django.urls import reverse

from crispy_forms.bootstrap import Tab, TabHolder
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field, Row, Column, Div
from crispy_forms.layout import Fieldset, ButtonHolder

from config.utils import get_new_key
from cutt.models import Cutt


class CuttCreateForm(forms.ModelForm):
    """
        Form for create new user redirection URL
    """
    class Meta:
        model = Cutt
        fields = ['url_original', 'url_short', 'key', 'session_key']

    url_original = forms.URLField(label='Original url', required=True)
    url_short = forms.URLField(label='Short url', required=True)
    key = forms.CharField(label='Short key', max_length=255, required=True)
    session_key = forms.CharField(max_length=32, required=False)

    def __init__(self, *args, **kwargs):
        new_key = get_new_key()
        self.helper = FormHelper()
        self.helper.form_action = reverse('cutt-create')
        self.helper.layout = Layout(
            Field('url_original', id="url", placeholder='Enter original URL', type=forms.UUIDField, required=True),
            Row(
                Div(
                    Field('url_short', readonly=True),
                    css_class='formColumn form-group col-md-8 mb-0',
                ),
                Div(
                    Field('key', value=new_key, placeholder='Value between 8 and 255 characters'),
                    css_class='formColumn form-group col-md-4 mb-0',
                ),
            ),
            Submit('submit', 'Create short redirect url'),
        )
        super(CuttCreateForm, self).__init__(*args, **kwargs)

