import logging

from django.views.generic import CreateView, ListView
from django.http import HttpResponseRedirect

from django.core.exceptions import ValidationError

from cutt.models import Cutt
from cutt.forms import CuttCreateForm


logger = logging.getLogger(__name__)


class CuttIndexView(CreateView):
    """
        Root page view, route: <site url>/
    """
    template_name = 'cutt/index.html'

    def get(self, request, *args, **kwargs):
        if not request.session.session_key:
            request.session.create()
        return self.render_to_response({})


class CuttCreateView(CreateView):
    """
        Create redirect view, route <site url>/cutt/create/
        Load on activate tab 'create'
    """
    template_name = 'cutt/create.html'

    def get(self, request, *args, **kwargs):
        form = CuttCreateForm()
        return self.render_to_response({'form': form})

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        data['session_key'] = request.session.session_key
        form = CuttCreateForm(data)
        if not form.is_valid():
            logger.info("post no valid from, data: '{}'".format(data))
            return self.render_to_response({'errors': form.errors})

        if Cutt.exist_key(form.cleaned_data['key']):
            logger.info("Current URL key is exist, data: '{}'".format(data))
            raise ValidationError('Current URL key is exist')

        form.save()
        logger.debug("create new redirect URL, data '{}'".format(data))

        return HttpResponseRedirect("/")


class CuttListView(ListView):
    """
        Create redirect view, route <site url>/cutt/list/
        Load on activate tab 'list'
    """
    template_name = 'cutt/list.html'
    paginate_by = 3

    def get_queryset(self):
        return Cutt.objects.filter(session_key=self.request.session.session_key).order_by('-id')

    def get_context_data(self, *args, object_list=None, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        return context


