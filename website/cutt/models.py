from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.cache import cache


class Cutt(models.Model):
    """
        Model for user redirect URLs
        Meaning of fields:
        url_original - original user URL
        url_shot - user preset URL
        key - unique key on url_short
        session_key - user session key
    """
    url_original = models.URLField()
    url_short = models.URLField()
    key = models.CharField(max_length=255, unique=True)
    session_key = models.CharField(max_length=32)

    @staticmethod
    def exist_key(key):
        """
        Check exists key for generation short_url
        :param key: short url key
        :return: boolean
        """
        if Cutt.objects.filter(key=key).first():
            return True
        return False


@receiver(post_save, sender=Cutt)
def update_redis_cache(sender, instance, created, **kwargs):
    """
    Update URL cache when create a new record
    :param sender:
    :param instance:
    :param created:
    :param kwargs:
    :return:
    """
    if created:
        cache.set(instance.key, instance.url_original)
