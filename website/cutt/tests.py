from django.urls import reverse
from django.test import TestCase


class CuttTestCase(TestCase):

    def setUp(self):
        super().setUp()

    def test_IndexView(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_CreateView(self):
        # GET
        import pudb; pudb.set_trace()
        response = self.client.get(reverse('cutt-create'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['form'].fields), 4)
        self.assertEqual(response.context['form_action'], reverse('cutt-create'))
        self.assertEqual(response.context['form_method'], 'post')

