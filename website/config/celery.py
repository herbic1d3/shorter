from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
from celery.schedules import crontab

from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('config')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'flush_redirect_cache': {
        'task': 'config.tasks.flush_redirect_cache',
        'schedule': crontab(minute='*/1', hour='*', day_of_week='*'),
    },
}


