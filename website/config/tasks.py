from celery.utils.log import get_task_logger


logger = get_task_logger('celery')


def flush_redirect_cache():
    """
    Flush redirect tasks on celery event
    :return:
    """
    from django_redis import get_redis_connection
    get_redis_connection("default").flushall()
    logger.info("flush redirect cache")

