from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from cutt.views import CuttIndexView, CuttCreateView, CuttListView
from redirection.views import RedirectionView
from advert.views import AdvertIndexView

urlpatterns = [
    path('cutt/create/', CuttCreateView.as_view(), name='cutt-create'),
    path('cutt/list/', CuttListView.as_view(), name='cutt-list'),
    path('a/<key>/', RedirectionView.as_view(url='%(key)s'), name='redirection'),
    path('advert/', AdvertIndexView.as_view(), name='advert'),
    path('', CuttIndexView.as_view(), name="index"),
]

urlpatterns += staticfiles_urlpatterns()
