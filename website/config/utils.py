import random
import string

from cutt.models import Cutt


def generate_unique_key():
    """
    generate random string key, number of combinations 3 381 098 545
    :return: str
    """
    letters = string.ascii_letters + string.digits
    return ''.join(random.choice(letters) for i in range(8))


def get_new_key():
    """
    Return unique key for URL generation
    :return: str
    """
    key = generate_unique_key()
    while Cutt.exist_key(key):
        key = generate_unique_key()
    return key

