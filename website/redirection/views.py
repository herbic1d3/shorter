import logging

from django.urls import reverse
from django.core.cache import cache
from django.views.generic.base import RedirectView

from cutt.models import Cutt


logger = logging.getLogger(__name__)


class RedirectionView(RedirectView):
    """
        Redirect preset URLs.
        If route URL is not exist, when redirect on advert page
    """
    permanent = False

    def get_redirect_url(*args, **kwargs):
        key = kwargs['key']
        url_original = cache.get(key)
        if url_original is None:
            obj = Cutt.objects.filter(key=key).first()
            if obj is None:
                url_original = reverse('advert')
            else:
                url_original = obj.url_original
            cache.set(key, url_original)
        logger.debug("redirect by key '{}' to '{}'".format(key, url_original))
        return url_original


