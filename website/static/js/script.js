(function ($) {
    // ---
    var min_key_length = 8;

    // ---
    function bind_on_pagination() {
        $(".page-links").click(function(e){
            e.preventDefault();
            tab_load($(".page-links").find("a").attr('href'), '#list');
        });
    }
    function bind_on_tab_load(target) {
        if (target === '#create') {
            $("#id_key").bind("change paste keyup", check_key);
            update_short_url($("#id_key"));
        }
    }
    function bind_on_page_load() {
        $(".nav-tabs a").click(function(e){
            e.preventDefault();
            tab_load($(this).attr('href'), $(this).attr('data-target'));
            $(this).tab('show');
        });
    }

    // ---
    function update_short_url(obj) {
        $('#id_url_short').val(window.location.origin + '/a/' + obj.val());
    }
    function check_key() {
        if ($(this).val().length < min_key_length) {
            $(this).css("background-color"," #ffe5cc");
        } else {
            $(this).css("background-color", "#fff");
        }
        update_short_url($(this));
    }
    // --- 
    function tab_load(url, target) {
        $.get(url, function (data) {
            $(target).html(data);
            bind_on_tab_load(target);
            bind_on_pagination();
        })
    }
    function initialization() {
        var obj = $("a[data-target=\"#create\"]");
        tab_load($(obj).attr('href'), $(obj).attr('data-target'));
    }

    // ---
    $(document).ready(function(){
        bind_on_page_load();
    });

    // ---
    initialization();
})(jQuery);
