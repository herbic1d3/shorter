#!/bin/bash
# Migrate the database first
cd /app/

export DJANGO_SETTINGS_MODULE="config.settings"

# create logs dir if on exists (first start)
if [[ ! -e /app/logs ]]; then
    mkdir -p /app/logs/
fi

if [[ ! -e /app/migrated.tmp ]]; then
    echo "Migrating the database before starting the server"
    python manage.py makemigrations
    python manage.py migrate

    if [[ $? -eq  0 ]]; then
        touch /app/migrated.tmp
    else
        # when mysql not start before migrate
        sleep 30
        exit
    fi
fi

# Start processes
gunicorn config.wsgi:application --bind 0.0.0.0:8000 --workers 3

